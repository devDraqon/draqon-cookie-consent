import React from "react";
import CookieBannerIntro from './CookieBannerIntro';

export default {
  title: "CookieBannerIntro"
};

const cookies = {
  list: [
    {
      name: "Color Theme",
      isActive: false,
      isThirdParty: "first",
      description: "Used to remember the color theme used in this website."
    },
    {
      name: "Username",
      isActive: false,
      isThirdParty: "first",
      description: "Used to greet the users more personally, when they enter the website."
    },
    {
      name: "Current Volume",
      isActive: false,
      isThirdParty: "first",
      description: "Used to store the current volume, so audio will never start too loud or too quiet after you tweaked it once."
    },
    {
      name: "Watch History",
      isActive: false,
      isThirdParty: "first",
      description: "Used to let users finish to watch a video which they started to watch. This Cookie may gets copied for each video."
    }
  ]
}

export const Default = () => <CookieBannerIntro optionalLogoSrc="https://files.draqondevelops.com/images/logodragon.png" cookies={cookies} hasRouter={false} />
