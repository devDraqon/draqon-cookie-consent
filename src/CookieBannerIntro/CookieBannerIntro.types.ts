export interface CookieBannerIntroProps {
    hasRouter: boolean;
    cookies: {
        list: Array<{
            name: string;
            isActive: boolean;
        }>
    };
    optionalLogoSrc?: string;
}