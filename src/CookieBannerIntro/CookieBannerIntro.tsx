import React, {useState, useEffect} from "react";
import {Headline, Paragraph, Button, Modal, Flex, Image} from "draqon-component-library";
import {ScrollSmoothly} from "draqon-modules";
import {CookieBannerIntroProps} from "./CookieBannerIntro.types";
import CookieBannerPreferences from "../CookieBannerPreferences/CookieBannerPreferences";

import "./CookieBannerIntro.scss";

const CookieBannerIntro = ({hasRouter, cookies, optionalLogoSrc}: CookieBannerIntroProps) => {

    const cookieWhitelistExists = () => document.cookie.match(/^(.*;)?\s*cookie_whitelist\s*=\s*[^;]+(.*)?$/) === null ? false : true;

    const [isSelfOpen, setIsSelfOpen] = useState(false);
    const [isChildOpen, setIsChildOpen] = useState(false);
    const [isMount, setIsMount] = useState(false);

    const closeBanner = () => {
        setIsSelfOpen(false);
        setIsChildOpen(false);
        ScrollSmoothly();
    }

    const openBanner = () => {
        setIsSelfOpen(true);
    }

    const openPreferences = () => {
        setIsChildOpen(true);
    }

    const askLater = () => {
        console.log("User choose to decide wether to use cookies or not later.");
        closeBanner();
    }

    const closePreferences = () => {
        ScrollSmoothly();
        setIsChildOpen(false);
    }

    useEffect(() => {
        console.log("following cookies has been passed onto the draqon-consent-cookie banner:")
        console.log(cookies)
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth"
        })
        if(!cookieWhitelistExists()) openBanner();
    }, [])

    useEffect(() => {
        if(!isMount) setIsMount(true)
        else
            if(isSelfOpen) {
                document.body.classList.add("noscroll");
                if(hasRouter) {
                    document.getElementsByClassName("router")[0].classList.add("noscroll");
                    document.getElementsByClassName("router")[0].classList.add("blur");
                }
            }
            else {
                document.body.classList.remove("noscroll");
                if(hasRouter) {
                document.getElementsByClassName("router")[0].classList.remove("noscroll");
                document.getElementsByClassName("router")[0].classList.remove("blur");
                }
            }
    }, [isSelfOpen])

    return (
        <span>{[
        isSelfOpen ?  
            <Modal callbackOnClose={closeBanner} position="top" key="banner_0" classList={[isChildOpen ? "cookiebanner__intro noscroll blur" : "cookiebanner__intro"]}>
                {optionalLogoSrc ? <Image src={optionalLogoSrc} alt="logodragon" /> : null}
                <Headline size={"large"} children="This website is using Cookies!"/>
                <Paragraph size="medium">
                    This website is using Cookies.
                    Please choose if you wish to accept or deny them.
                    You can also choose to make your decission later.
                </Paragraph>
                <Flex direction="horizontal">
                    <Button value="Ask me later" callbackOnClick={askLater} />
                    <Button value="Open Preferences" callbackOnClick={openPreferences} />
                </Flex>
            </Modal> : null,
            isChildOpen ? <CookieBannerPreferences optionalLogoSrc={optionalLogoSrc} cookies={cookies} key="banner_1" closeBanner={closeBanner} closePreferences={closePreferences} /> : null
        ].map((returnItem) => returnItem)}</span>
    );
}

export default CookieBannerIntro;