export interface CookieProps {
    toggleAllow: Function | void | any;
    name: string;
    id: number;
    isActive: boolean;
    description?: string;
    isThirdParty?: "first" | "third";
}