import React, {useState, useEffect} from "react";
import {Headline, Switch, Paragraph} from "draqon-component-library";
import {CookieProps} from "./Cookie.types";
import "./Cookie.scss";

const Cookie = ({toggleAllow, isThirdParty, name, isActive, description, id}: CookieProps) => {
    return (
        <div className="cookie">
            <div className="cookie__head">
                <div className="cookie__head-upper">
                    <Switch isActive={isActive} callbackOnClick={() => toggleAllow(id)} />
                    <Paragraph size={"medium"} children={name} />
                </div>
                {isThirdParty ? <div className="cookie__head-lower">
                    <Paragraph size="small"> [{isThirdParty === "third" ? "Third" : "First"} Party Cookie] </Paragraph> 
                </div> : null}
            </div>
            {description ? 
            <div className="cookie__description">
                <Paragraph size="small"> {description} </Paragraph> 
            </div> : null}
        </div>
    )
}

export default Cookie;