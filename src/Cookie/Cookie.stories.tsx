import React, {useState} from "react";
import Cookie from './Cookie';

export default {
  title: "Cookie"
};

export const WithoutAdditionalInfo = () => {
    const [isActive, setIsActive] = useState(false);
    return <Cookie name="Cookie" id={0} isActive={isActive} toggleAllow={() => setIsActive(!isActive)}/>;
}

const description =  "Lorem Ipsum solom dir Lorem Ipsum solom dirLorem Ipsum solom dirLorem Ipsum solom dirLorem Ipsum solom dir Lorem Ipsum solom dir Lorem Ipsum solom dir Lorem Ipsum solom dir"

export const WithDescription = () => {
    const [isActive, setIsActive] = useState(false);
    return <Cookie description={description} name="Cookie" id={0} isActive={isActive} toggleAllow={() => setIsActive(!isActive)}/>;
}
export const WithHolder = () => {
    const [isActive, setIsActive] = useState(false);
    return <Cookie isThirdParty="first" name="Cookie" id={0} isActive={isActive} toggleAllow={() => setIsActive(!isActive)}/>;
}


export const WithDescriptionAndHolder = () => {
    const [isActive, setIsActive] = useState(false);
    return <Cookie isThirdParty="first" description={description} name="Cookie" id={0} isActive={isActive} toggleAllow={() => setIsActive(!isActive)}/>;
}
