import React from "react";
import CookieBannerPreferences from './CookieBannerPreferences';

export default {
  title: "CookieBannerPreferences"
};

const cookies = {
  list: [
    {
      name: "First Cookie",
      isActive: false,
      description: "Lorem Ipsum solom dir Lorem Ipsum solom dirLorem Ipsum solom dirLorem Ipsum solom dirLorem Ipsum solom dir Lorem Ipsum solom dir Lorem Ipsum solom dir Lorem Ipsum solom dir"
    },
    {
      name: "Second Cookie",
      isThirdParty: "first",
      isActive: false,
    },
    {
      name: "Third Cookie",
      isThirdParty: "first",
      isActive: false,
      description: "Lorem Ipsum solom dir Lorem Ipsum solom dirLorem Ipsum solom dirLorem Ipsum solom dirLorem Ipsum solom dir Lorem Ipsum solom dir Lorem Ipsum solom dir Lorem Ipsum solom dir"
    },
    {
      name: "Fourth Cookie",
      isActive: false,
    }
  ]
}

export const Default = () => <CookieBannerPreferences optionalLogoSrc="https://files.draqondevelops.com/images/logodragon.png" cookies={cookies} closePreferences={null} closeBanner={null}  />
