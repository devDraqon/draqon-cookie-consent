export interface CookieBannerPreferencesProps {
    closePreferences: Function | void,
    closeBanner: Function | void | any,
    cookies: any,
    optionalLogoSrc?: string;
}