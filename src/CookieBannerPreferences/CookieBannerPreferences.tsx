import React, {useState, useEffect} from "react";
import {Headline, Paragraph, Button, Modal, Flex, Image} from "draqon-component-library";
import {GetExpireDate} from "draqon-modules";
import {CookieBannerPreferencesProps} from "./CookieBannerPreferences.types";
import Cookie from "../Cookie/Cookie";
import "./CookieBannerPreferences.scss";

const CookieBannerPreferences = ({closePreferences, closeBanner, cookies, optionalLogoSrc}: CookieBannerPreferencesProps) => {

    const [cookiesState, setCookies]: any = useState(cookies)

    const savePreferences = () => {
        document.cookie = "cookie_whitelist="+ JSON.stringify(cookies)+";expires="+GetExpireDate().toUTCString()+";";
        closeBanner();
    }

    const toggleCookieAllow = (id: number) => {
        setCookies({list: cookiesState.list.map((cookie: any, iteratedCookieID: number) => {
            if(iteratedCookieID === id) cookie.isActive = !cookie.isActive;
            console.log(cookie.isActive)
            return cookie;
        })})
    }

    const allowAll = () => {
        setCookies({list: cookiesState.list.map((cookie: any) => {
            cookie.isActive = true;
            return cookie;
        })})
    }
    
    const denyAll = () => {
        setCookies({list: cookiesState.list.map((cookie: any) => {
            cookie.isActive = false;
            return cookie;
        })})
    }

    return (
        <Modal callbackOnClose={closePreferences} position="center" classList={["cookiebanner__preferences"]}>
            <div className="cookiebanner__preferences-header">
                {optionalLogoSrc ? <Image src={optionalLogoSrc} alt="logodragon" /> : null}
                <Headline size="medium" children="Update your Cookie Preferences!"/>
                <Paragraph size="medium">
                    Here you can update your cookie preferences.
                    Simply use the slider on the right side of each of the cookies.
                    When you are done, simply click on save Preferences and the cookie banner will close.
                    Alternatively, you can go back and choose to update your preferences later.
                </Paragraph>
            </div>
            
            <div className="cookiebanner__preferences-cookielist">
                <Headline size={"medium"} children="Cookie List" />
                {cookiesState.list.map((cookie: any, id: number) => /* When rendering cookies, take care of optional parameters in the cookielist and render them respectively to their existance. */ 
                    cookie.isThirdParty && cookie.description ? <Cookie isThirdParty={cookie.isThirdParty} description={cookie.description} id={id} key={id} name={cookie.name} isActive={cookie.isActive} toggleAllow={toggleCookieAllow}/> 
                    : cookie.isThirdParty && !cookies.description ? <Cookie isThirdParty={cookie.isThirdParty} id={id} key={id} name={cookie.name} isActive={cookie.isActive} toggleAllow={toggleCookieAllow}/> 
                    : !cookie.isThirdParty && cookies.description ? <Cookie description={cookie.description} id={id} key={id} name={cookie.name} isActive={cookie.isActive} toggleAllow={toggleCookieAllow}/> 
                    : !cookie.isThirdParty && !cookies.description ? <Cookie id={id} key={id} name={cookie.name} isActive={cookie.isActive} toggleAllow={toggleCookieAllow}/> : null
                )}
            </div>

            <Flex direction="horizontal">
                <Button value="Allow all" callbackOnClick={allowAll} />
                <Button value="Deny all" callbackOnClick={denyAll} />
            </Flex>
                <Button value="Save Preferences" callbackOnClick={savePreferences} />
                {/*<Button value="Go Back" callbackOnClick={closePreferences} />*/}
        </Modal>
    );
}

export default CookieBannerPreferences;