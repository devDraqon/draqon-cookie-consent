# Draqons Cookie Consent
This module makes it easier to handle cookies 
legally compliant and comes with a 2-layer-deep cookiebanner which follows best-practises in design.

## Live Demo
<a href="https://storybook.draqondevelops.com/react-cookie-consent"> click here </a> to visit a static live version of this module.

## Installation
    npm install --save draqon-cookie-consent

## Syntax
The Cookie banner will require you to create a JSON object with the key **list**.  
The **list** represents all cookies which your website has whitelisted.  
*please note* each cookie which is not whitelisted, will be blocked by this module for security reasons in a future version.

of cookies you'd like to allow. In that list, each cookie must have a **name** and can be set to inactive by the user(**isActive**). optionally, you can also specify the holder(**isThirdParty**) of each cookie and pass a **description** to it.

### Importing Cookie Banner
    import CookieBanner from 'draqon-cookie-consent';

### Example Syntax #2
    const cookies = {
        list: [
            {   /* First Party Cookie with all optional parameters */
                name: "First Cookie",
                isActive: false,
                isThirdParty: "first",
                description: "Lorem Ipsum solom dir"
            },
            {
                /* Third Party Cookie with all optional parameters */
                name: "Second Cookie",
                isActive: false,
                isThirdParty: "third",
                description: "Lorem Ipsum solom dir"
            },
            {   /* Cookie without Description */
                name: "First Cookie",
                isActive: false,
                isThirdParty: "first",                  
            },
            {
                /* Cookie without First/Third Party Label */
                name: "Second Cookie",
                isActive: false,
                description: "Lorem Ipsum solom dir"
            }
            {   /* Cookie without Description and Without First/Third Party Label */
                name: "Second Cookie",
                isActive: false,
            }
        }
    ]

### Rendering Cookie Banner
    const TestComponent = () => {
        return( 
            <CookieBanner cookies={cookies} hasRouter={false} />
        )
    }

### Source Code
<a href="https://gitlab.com/devdraqon/draqon-cookie-consent"> visit the source code for this project here. </a>

### Known Bugs
- Yet also function cookies can be disabled by the user. Version 1.2.0 will include an optional prperty for function cookies, which cannot be disabled, but seen by the user.

### Report Bugs and Issues
If you encounter any bugs, issues, or have improvements which you'd like to share, please choose one of the following ways to report your issue.
- <a href="https://gitlab.com/devDraqon/draqon-cookie-consent/-/issues"> Report Issue on Gitlab </a> (requires having a gitlab account).
- <a href="mailto:draqondevelops@gmail.com"> Mail your Issue </a> to me.

### Version History
-   Start of documentated version history 