const path = require("path");

module.exports = {
  core: {
    builder: "webpack5",
  },
  stories: ["../src/**/*.stories.tsx"],
  // Add any Storybook addons you want here: https://storybook.js.org/addons/
  addons: [],
  webpackFinal: async (config) => {
    config.module.rules.push({
      test: /\.scss$/,
      use: ["style-loader", "css-loader", "sass-loader"],
      include: path.resolve(__dirname, "../")
    });

    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      loader: require.resolve("babel-loader"),
      options: {
        presets: [["react-app", { flow: false, typescript: true }]]
      }
    });

    config.module.rules.push({
      test: /\.(png|woff|woff2|eot|ttf|svg)(\?.*$|$)/,
      include: path.resolve(__dirname, '../'),
      use: [
        {
          loader: 'file-loader',
        }
      ],
    });

    config.resolve.extensions.push(".ts", ".tsx");

    return config;
  }
};